SHELL := /bin/bash

# Install Calamares Settings Mobian Juno

DESTDIR=debian/calamares-settings-mobian-juno

install-core:
	install -dm755 $(DESTDIR)/etc
	install -dm755 $(DESTDIR)/usr/lib/calamares/modules
	install -dm755 $(DESTDIR)/usr/sbin
	install -dm755 $(DESTDIR)/usr/bin
	install -dm755 $(DESTDIR)/usr/share/applications
	install -dm755 $(DESTDIR)/usr/share/pixmaps
	install -dm755 $(DESTDIR)/usr/share/glib-2.0/schemas/
	install -dm755 $(DESTDIR)/etc/xdg/autostart
	install -dm755 $(DESTDIR)/etc/systemd/system/
	cp -R calamares $(DESTDIR)/etc/
	cp -R calamares-modules/* $(DESTDIR)/usr/lib/calamares/modules/
	cp scripts/* $(DESTDIR)/usr/sbin
	install -Dpm 0644 calamares-install-debian.desktop $(DESTDIR)/usr/share/applications/calamares-install-debian.desktop
	install -Dpm 0755 install-debian $(DESTDIR)/usr/bin/install-debian
	install -Dpm 0644 artwork/install-debian.png $(DESTDIR)/usr/share/pixmaps/install-debian.png
	install -Dpm 0644 conf/96_calamares-settings-debian.gschema.override $(DESTDIR)/usr/share/glib-2.0/schemas/96_calamares-settings-debian.gschema.override
	install -Dpm 0644 conf/calamares-desktop-icon.desktop $(DESTDIR)/etc/xdg/autostart/calamares-desktop-icon.desktop
	install -Dpm 0755 add-calamares-desktop-icon $(DESTDIR)/usr/bin/add-calamares-desktop-icon
	install -Dpm 0644 calamares-resolv.service $(DESTDIR)/etc/systemd/system/calamares-resolv.service
	install -Dpm 0644 calamaresfb.service $(DESTDIR)/etc/systemd/system/calamaresfb.service

install: install-core
	
uninstall:
	rm -R $(DESTDIR)/etc/calamares
	rm -R $(DESTDIR)/usr/lib/calamares/modules/
	rm -f $(DESTDIR)/usr/sbin/bootloader-config
	rm -f $(DESTDIR)/usr/sbin/dpkg-unsafe-io
	rm -f $(DESTDIR)/usr/sbin/sources-final
	rm -f $(DESTDIR)/usr/sbin/sources-media
	rm -f $(DESTDIR)/usr/share/applications/calamares-install-debian.desktop
	rm -f $(DESTDIR)/usr/bin/install-debian
	rm -f $(DESTDIR)/usr/share/pixmaps/install-debian.png
	rm -f $(DESTDIR)/usr/share/glib-2.0/schemas/96_calamares-settings-debian.gschema.override
	rm -f $(DESTDIR)/etc/xdg/autostart/calamares-desktop-icon.desktop
	rm -f $(DESTDIR)/usr/bin/add-calamares-desktop-icon
	rm -f $(DESTDIR)/etc/systemd/system/calamares-resolv.service
	rm -f $(DESTDIR)/etc/systemd/system/calamaresfb.service
